# Comics

[![](images/general_store48.png)](index.html)

Comics on a variety of topics made in good fun.

- 2021.03.28 [MS Excel](./comics/devStories_2021.03.28.html)
- 2021.02.07 [Dependencies](./comics/devStories_2021.02.07.html)
- 2021.02.01 [ASP.NET](./comics/devStories_2021.02.01.html)
- 2021.01.28 [Git Merge vs Rebase](./comics/devStories_2021.01.28.html)
- 2021.01.20 [Insanity](./comics/devStories_2021.01.20.html)
