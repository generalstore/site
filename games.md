# Games

[![](images/general_store48.png)](index.html)


[LD38: The Grass is Always Greener](https://gitlab.com/generalstore/thegrassisalwaysgreener)

<a href="https://gitlab.com/generalstore/thegrassisalwaysgreener"><img src="images/giag.png" height="48" width="48" ></a>

- Theme: A Small World
- 2D platformer, event driven story

[LD36: The Collector](https://gitlab.com/generalstore/thecollector)

<a href="https://gitlab.com/generalstore/thecollector"><img src="images/collector.png"></a>

- Theme: Ancient Technology
- 2D top down, dungeon crawler, player v. environment, collect the artifacts

[LD34: Wayward Souls](https://gitlab.com/generalstore/waywardsouls)

<a href="https://gitlab.com/generalstore/waywardsouls"><img src="images/waywardSouls.png" height="48" width="48" ></a>

- Theme: Growth
- 2D platformer, event driven story

---

- [LudumDare (LD)](https://ldjam.com/) - A 48 hour game development competition to build a game from scratch while adhering to an elected theme.
