# Site

Personal programming projects website hosted on github.

## Building

### Dependencies

- requires Lua 5.3.x or higher to be in the path
- requires Pandoc to be in the path
- requires the latest from the [LuaModules]() repository
- requires LuaModules repository to be accessible from LUA_PATH

*build.lua* provides several commands to be able to build the website.


### Build

This will perform a full rebuild of the site - generating all the appropriate html files from the markdown files.

```bat

lua build.lua -b

```

