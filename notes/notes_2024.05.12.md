# Python venv

*2024.05.12*

Windows:

```
[~> python -m venv _env
[~> .\_env\scripts\activate.bat
[~/_env/>

[~/_env/> .\env\scritps\deactivate.bat
[~>
```

Linux:

```
[~> python -m venv _env
[~> source ./_env/bin/activate
[~/_env/>

[~/_env/>deactivate
[~>
```

