# C++/CLI Static Classes

*2023.04.20*

- Abstract Sealed == Static
- [System::Runtime::InteropServices::OutAttribute] == out

Definition:

```cpp

public ref class ServiceEx abstract sealed
{
public:
	static StatusCode Initialize();
	static StatusCode GetStatus([System::Runtime::InteropServices::OutAttribute] ServiceStatus^% status);
	// ...
}

```

Managed C# code calling in the C++/CLI static service:

```cs

	StatusCode result = ServiceEx.GetStatus(out ServiceStatus status);

```

