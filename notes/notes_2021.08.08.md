# Windows del and backslash escaping

*2021.08.08*

I discovered an interesting behavior with the Windows command del / erase. 

Assuming you had the following directory structure and an empty file called 'file1.txt' as listed below. This command will fail if you were to execute it.

```Bat
del "C:\\_Dev\\TestFolder\\file1.txt"
```

It appears that unlike a lot of other Windows commands such as 'rmdir', del is not able to handle the escaped backslash.

What's the significance of this? It's not really significant. I stumbled across it while making some new modules for LuaModules, an entertaining side project of mine centered around the idea of providing 'pure' LuaModules for a lot of basic workflows such as file system operations. In this case the issue was exposed in the implementation of the file.lua module when attempting to delete a file. Since it's supposed to be a 'pure' Lua module (no usage of C/C++ Lua modules), it's making use of the io.popen to execute command line commands; in this case using the del command for deleting of files. Since Lua requires the backslash '\\' to be escaped this means the command sent to popen looks like the example above and fails saying the command is unable to locate the specified file.

```Lua
io.popen(string.format('del /F /Q "%q"', file_path))
```

To work around this, I simply used the powershell Remove-Item command instead, which is much slower, but it is able to handle the escaped backslash file path. 

```Lua
io.popen(string.format('powershell "Remove-Item %q -Force"', file_path))
```

Side Note: I do not recommend using this technique of invoking commands from scripts, especially in production software. It would be more robust and reliable to use C/C++ Lua modules such as LuaFileSystem for handling such workflows. This LuaModules project of mine is simply a goofy side-project mostly for learning purposes and is not intended to be used outside of toy projects.


