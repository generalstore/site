# C++/CLI Eliminating Unsafe

*2023.04.21*


The following code will result in the generation of an unsafe static method GetStatus.

```cpp

// Service.h

public ref class ServiceEx abstract sealed
{
public:
	static StatusCode GetStatus([System::Runtime::InteropServices::OutAttribute] ServiceStatus^% status);
}

```

```cpp

// Service.cpp

StatusCode ServiceEx::GetStatus([System::Runtime::InteropServices::OutAttribute] ServiceStatus^% status)
{
	status = gcnew ServiceStatus();

	NativeServiceStatus nativeStatus;
	try
	{
		NativeServiceStatusCode c = NativeService.GetStatus(nativeStatus);
	
		status->IsInitialized = nativeStatus.IsInitialized;
		status->IsRunning = nativeStatus.IsRunning;
	}
	catch (...)
	{
		return StatusCode::Failed;
	}
	return StatusCode::Success;
}

```

```cpp
// nativeService.h

class NativeService
{
public:
	static NativeServiceStatusCode GetStatus(NativeServiceStatus& nativeStatus);
}

```

Results in the following managed assembly interface:
```cs

public static class ServiceEx
{
	static unsafe StatusCode GetStatus([System::Runtime::InteropServices::OutAttribute] ServiceStatus^% status);
}


```

However, moving the code where we pass a native reference to GetStatus Native function into another function, we are able to eliminate the unsafe keyword.

```cpp
// service.cpp

bool FetchStatus(ServiceStatus^% status)
{
	NativeServiceStatus nativeStatus;
	try
	{
		NativeServiceStatusCode c = NativeService.GetStatus(nativeStatus);
	
		status->IsInitialized = nativeStatus.IsInitialized;
		status->IsRunning = nativeStatus.IsRunning;
	}
	catch (...)
	{
		return false;
	}
	return true;
}

StatusCode ServiceEx::GetStatus([System::Runtime::InteropServices::OutAttribute] ServiceStatus^% status)
{
	status = gcnew ServiceStatus();
	if (!FetchStatus(status))
		return StatusCode::Failure;

	return StatusCode::Success;
}

```

Results in the following interface:
```cs
public static class ServiceEx
{
	static StatusCode GetStatus([System::Runtime::InteropServices::OutAttribute] ServiceStatus^% status);
}

```


