# Manually Uploading Artifacts to ADO

*2024.05.14*

Create a feed if you have not already done so. Keep in mind at which scope the feed is created (organization or project level). In this example the project scope is used.

Ensure the Azure CLI is installed.

Create a personal access token with Package Read & Write permissions enabled.

```
az login
token: ******
az artifacts universal publish --organization <org> --project <proj> --scope project --feed <feed> --name <name_lower_no_special_chars> --version 0.0.1 --path <directory path>
```


