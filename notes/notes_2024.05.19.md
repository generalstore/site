# Name mangling exported functions in C++ DLLs

*2024.05.19*

When building a DLL the function name exported may not match the name you expect unless care is taken to prevent name mangling by using the following:

```c++
#ifdef _WIN32
	#define DLL_EXPORT __declspec(dllexport)
#else
	#define DLL_EXPORT
#endif

// Use extern "C" block to prevent name mangling
extern "C" 
{
	DLL_EXPORT int Add(int a, int b)
	{
		return a + b;
	}	
}

```

The dumpbin.exe tool can be used to see what functions are exported in a DLL:

```
dumpbin /exports TargetLibrary.dll
```
