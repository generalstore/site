# CallerMemberNameAttribute

*2021.09.21*

Looking through the Prism framework, I stumbled across: [CallerMemberNameAttribute](https://docs.microsoft.com/en-us/dotnet/api/system.runtime.compilerservices.callermembernameattribute?view=net-5.0).

Prism used it in the context of updating a bound UI property:

```cs

	private string _name;
	public string Name
	{
		get { return _name; }
		set 
		{
			SetProperty(ref _name, value);
		}
	}

	// SetProperty<T>(ref T v, val, [CallerMemberName] propertyName = null)

```

which eliminates the need to type out the property name when notifying the view to update. The replacement occurs at compile time so there is no runtime performance cost.

```cs
	private string _name;
	public string Name
	{
		get { return _name; }
		set
		{
			_name = value;
			NotifyPropertyChanged(nameof(Name));
		}
	}
```

