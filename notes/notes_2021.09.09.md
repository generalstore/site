# Odd git branch name collision

*2021.09.09*

Let's assume you have the following git branch available:

```
release
```

Now let's try to create another branch named:

```
release/fix_2910
```

This will not be permitted since the branch 'release' already exists. Ran into this the other day. It is an interesting limitation on the branch naming.


