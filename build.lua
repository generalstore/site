local args = require('args')
local path = require('path')
local file = require('file')
local ose = require('os_ext')
local str = require('str')

local function print_help()
	local str_format = "%-15s | %-3s | %-25s | %-60s"
	print(string.format(str_format, "Argument", "Req", "Flags", "Description"))
	print(str.rep('-', 100))
	for i, v in ipairs(args._cmds) do
		local required_str = ' '
		if v.required then
			required_str = '.'
		end
		local flag_str = table.concat(v.flags, ",  ")
		print(string.format(str_format, v.name, required_str, flag_str, v.help))
	end
end


local function new_note(note_folder, title)
    local date = os.date('*t')
    local year = tostring(date.year)
    local month = tostring(date.month)
    local day = tostring(date.day)
    if date.month < 10 then
        month = string.format('0%d', date.month)
    end
    if date.day < 10 then
        day = string.format('0%d', date.day)
    end
	local note_date_str = string.format('%s.%s.%s', year, month, day)
    local note_filename = string.format('%s_%s.md', note_folder, note_date_str)
    local new_note = io.open(string.format('./%s/%s', note_folder, note_filename),'w')
    new_note:write(string.format('# %s\n\n', title))
    new_note:write(string.format('*%s*\n\n', note_date_str))
    new_note:close()
    print(string.format('created new note: %s', note_filename))
end


local function fetch_title_from_file(file_path)
    local file = io.open(file_path, 'r')
    local title_line = file:read('l')
    file:close()
    local title_hash_pos = string.find(title_line, '#')
    if not title_hash_pos then
        return nil
    end
    local title = string.sub(title_line, title_hash_pos + 2, string.len(title_line) -  title_hash_pos + 1)
    return title
end

local function fetch_date_from_file_path(file_path)
    local pos = string.find(file_path, '_')
    if pos == nil then return nil end

	-- 2020.12.18 (10 chars)
    local date = string.sub(file_path, pos + 1, pos + 10)
    return date
end

local function generate_html_file(md_filepath, html_filepath, title, cssStyleFilePath)
    local command = string.format('pandoc --css %q -f markdown -t html --mathml -s -o %s %s --metadata pagetitle=%q -V "mainfont:Font-Regular.otf"', cssStyleFilePath, html_filepath, md_filepath, title)
    print(command)
    ose.run_command(command)
end

local function generate_notes(note_folder, note_md_file, cssStyleFilePath)
    local include_subdir = false
    local all_files = path.list_files(note_folder, include_subdir)
    local note_file_list = {}
    for k, v in ipairs(all_files) do
        if string.find(v, '.html') then
            file.delete(v)
        end
        table.insert(note_file_list, v)
    end
    table.sort(note_file_list, function(i, j) return i > j end) -- order by most recent note
	local template_file = string.format('template-%s', note_md_file)
	file.copy(template_file, note_md_file)
    for k, v in ipairs(note_file_list) do
        local title = fetch_title_from_file(v)
        if title == nil then
            goto continue
        end
        local date = fetch_date_from_file_path(v)
        local file_name_no_ext = string.sub(v, 1, string.len(v) - 3)
        local html_filepath = string.format('.%s%s.html', ose.path_separator, file_name_no_ext)
        local index_file = io.open(note_md_file, 'a+')
        index_file:write(string.format('- %s [%s](%s)\n', date, title, html_filepath))
        index_file:close()
        local md_filepath = string.format('.%s%s', ose.path_separator, v)
        generate_html_file(md_filepath, html_filepath, title, cssStyleFilePath)
        ::continue::
    end
end


local function run_build()
	local cssStyleFile = 'style.css'
	local relativeStyleFile = '../style.css'
	generate_notes('notes', 'notes.md', relativeStyleFile)
	generate_notes('comics', 'comics.md', relativeStyleFile)
	generate_notes('blog', 'blog.md', relativeStyleFile)
    
	generate_html_file('index.md', 'index.html', 'General Store', cssStyleFile)
    generate_html_file('tools.md', 'tools.html', 'Tools', cssStyleFile)
    generate_html_file('games.md', 'games.html', 'Games', cssStyleFile)
	generate_html_file('notes.md', 'notes.html', 'Notes', cssStyleFile)
	generate_html_file('blog.md', 'blog.html', 'Blog', cssStyleFile)
	generate_html_file('comics.md', 'comics.html', 'Comics', cssStyleFile)
end


args:add_command('note', 'string', {'-n', '--note'}, 1, false, 'Create new note')
args:add_command('blog', 'string', {'-bl', '--blog'}, 1, false, 'Create new blog entry')
args:add_command('comic', 'string', {'-c', '--comic'}, 1, false, 'Create new comic')
args:add_command('build', 'string', {'-b', '--build'}, 0, false, 'Run the build')
args:add_command('help', 'boolean', {'-h', '--help', '/?' }, 0, false, 'Display all available commands.')

local result, data = pcall(function() return args:parse(arg) end)
if not result then
	print(data)
	return
end

if data['help'] then
	print_help()
	return
end

if data['note'] then
    new_note('notes', data['note'][1])
end

if data['comic'] then
	new_note('comics', data['comic'][1])
end

if data['blog'] then
	new_note('blog', data['blog'][1])
end

if data['build'] then
    run_build()
end

