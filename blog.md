# Behind the keyboard

[![](images/general_store48.png)](index.html)

Stories, learning opportunities, prophecies, and omens from the software engineering industry.

- 2024.07.14 [Clumsy Code - For Switch Logic Sequence](./blog/blog_2024.07.14.html)
