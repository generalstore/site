# General Store

[![](images/general_store48.png)](index.html)

Welcome to the General Store! Feel free to take a look around and see if anything catches your eye.

- ![Blog](images/journal48.png)[Blog](blog.html)
- ![Notes](images/notes48.png)[Notes](notes.html)
- ![Comics](images/comicbook48.png)[Comics](comics.html)
- ![Games](images/controller48.png)[Games](games.html)
- ![Tools](images/toolbox48.png)[Tools](tools.html)
