# Tools

[![](images/general_store48.png)](index.html)


- [LuaModules](https://gitlab.com/generalstore/LuaModules) - Cross-platform ecosystem of pure Lua modules.

- [PySchemaModel](https://gitlab.com/generalstore/pyschemamodel) - Python module for simplifying data schemas definitions with built-in JSON validation and serialization.

- [Timer](https://gitlab.com/generalstore/timer) - Cross-platform console application to compute the execution time of scripts and programs.

- [Dice Tool](diceTool.html) - Web-based dice tool for tabletop games.

